## To do

### Document variables on `defaults/`
### CHANGELOG
- [ ] Set up a CHANGELOG.md
### CONTRIBUTING
- [ ] Set up a CONTRIBUTING.md
### Ansible Galaxy
- [ ] Update `meta/main.yml`
- [ ] Add repo to Ansible Galaxy

## In Progress

### README
- [ ] Update Logos
- [ ] Update Badges
- [x] Update Role Name and Summary
- [x] Document Requirements
- [ ] Document Role Variables
- [ ] Document Dependencies
- [ ] Document Example Playbook
- [ ] Document License
- [ ] Document Author Information

## Done

### LICENSE
- [x] Set up a LICENSE
